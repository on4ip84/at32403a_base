/*
 * Tasks.h
 *
 *  Created on: 2023 Apr 13
 *      Author: on4ip
 */

#ifndef USER_TASKS_H_
#define USER_TASKS_H_

/*** FREERTOS ****/
#include "FreeRTOS.h"
#include "task.h"
#define ENABLE_FREERTOS					1
void initFreeRtos(void);

#endif /* USER_TASKS_H_ */
