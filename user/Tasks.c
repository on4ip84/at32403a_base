/*
 * Tasks.c
 *
 *  Created on: 2023 Apr 13
 *      Author: on4ip
 */
#include "Tasks.h"
#include "at32f403a_407_board.h"
#include "CommInterface.h"
TaskHandle_t led2_handler;
/* led2 task */
void led2_task_function(void *pvParameters);


void initFreeRtos(void)
{
	/* enter critical */
#if ENABLE_FREERTOS
	taskENTER_CRITICAL();
	xTaskCreate((TaskFunction_t) led2_task_function, (const char*) "LED2_task",
			(uint16_t) 512, (void*) NULL, (UBaseType_t) 2,
			(TaskHandle_t*) &led2_handler);
	/* exit critical */
	taskEXIT_CRITICAL();

	/* start scheduler */
	vTaskStartScheduler();
#endif
}


void vApplicationIdleHook( void )
{
	messageLoop();
}

/* led2 task function */
void led2_task_function(void *pvParameters)
{
  while(1)
  {
    at32_led_toggle(LED2);
    vTaskDelay(1000);
  }
}
